﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoGameOverManager : MonoBehaviour {
	
	public static InfoGameOverManager Current;

	public Text gameOver;
	
	public InfoGameOverManager()
	{
		Current = this;
	}
	
	public void SetLine(string line1)
	{
		gameOver.text = line1;
	}
	
	public void ClearLine()
	{
		SetLine ("");
	}

	// Use this for initialization
	void Start () {
		ClearLine ();
	}
}
