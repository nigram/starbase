﻿using UnityEngine;
using System.Collections;

public class CreateDronesAi : AiBehavior {

	public int DronesPerBase = 5;

	public float cost = 25;

	private AiSupport support;

	public override float GetWeight ()
	{
		if (support == null)
			support = AiSupport.GetSupport (gameObject);

		if (support.Player.Credits < cost)
			return 0;

		var drones = support.Drones.Count;
		var bases = support.ComandBases.Count;

		if (bases == 0)
			return 0;

		if (drones >= bases * DronesPerBase)
			return 0;

		return 1;
	}

	public override void Execute ()
	{
		Debug.Log ("creating drone");

		var bases = support.ComandBases;
		var index = Random.Range (0, bases.Count);
		var commandBase = bases [index];

		commandBase.GetComponent<CreateUnitAction> ().GetClickAction () ();
	}
}
